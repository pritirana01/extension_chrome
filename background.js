// chrome.runtime.onInstalled.addListener(() => {
//   console.log('AI Extension Installed');
//   chrome.contextMenus.create({
//       id: 'aiExtension',
//       title: 'My AI FRIEND',
//       contexts: ['selection']
//   });
//   chrome.contextMenus.create({
//       id: 'ask',
//       parentId: 'aiExtension',
//       title: 'Ask',
//       contexts: ['selection']
//   })
//   chrome.contextMenus.create({
//       id: 'summarize',
//       parentId: 'aiExtension',
//       title: 'Summarize',
//       contexts: ['selection']
//   });
//   chrome.contextMenus.create({
//       id: 'explain',
//       parentId: 'aiExtension',
//       title: 'Explain',
//       contexts: ['selection']
//   });
// });
// chrome.contextMenus.onClicked.addListener((info, tab) => {
//   const selectedText = info.selectionText;
//   chrome.storage.local.set({ selectedText }, () => {
//       chrome.windows.create({
//           url: chrome.runtime.getURL('index.html'),
//           type: 'popup',
//           width: 350,
//           height: 450
//       });
//   });
// });

//   chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
//     if (message.type === 'PUSH_CONTENT') {
//       fetch('http://localhost:3000/push', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({ pageContent: message.pageContent, url: message.url })
//       })
//       .then(response => response.json())
//       .then(data => sendResponse(data))
//       .catch(error => sendResponse({ error: 'Failed to push content.' }));
//       return true;
//     }
//     if (message.type === 'SUMMARIZE_CONTENT') {
//       fetch('http://localhost:3000/summarize', { 
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({ pageContent: message.pageContent,url: message.url })
//       })
//       .then(response => response.json())
//       .then(data => sendResponse(data))
//       .catch(error => sendResponse({ error: 'Failed to fetch summary.' }));
//       return true;
//     }
//     if (message.type === 'EXPLAIN_CONTENT') {
//       fetch('http://localhost:3000/explain', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({ userInput: message.userInput, pageContent: message.pageContent })
//       })
//       .then(response => response.json())
//       .then(data => sendResponse(data))
//       .catch(error => sendResponse({ error: 'Failed to explain content.' }));
//       return true;
//     }
//     if (message.type === 'signup') {
//       fetch('http://localhost:3000/signup', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({ email: message.email, password: message.password })
//       })
//       .then(response => response.json())
//       .then(data => sendResponse(data))
//       .catch(error => sendResponse({ error: 'Signup failed.' }));
//       return true;
//     }
  
//     if (message.type === 'signinButton') {
//       fetch('http://localhost:3000/signin', {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({ email: message.email, password: message.password })
//       })
//       .then(response => response.json())
//       .then(data => sendResponse(data))
//       .catch(error => sendResponse({ error: 'Login failed.' }));
//       return true;
//     }


//   });

chrome.runtime.onInstalled.addListener(() => {
  console.log('AI Extension Installed');
  chrome.contextMenus.create({
      id: 'aiExtension',
      title: 'My AI FRIEND',
      contexts: ['selection']
  });
  chrome.contextMenus.create({
      id: 'ask',
      parentId: 'aiExtension',
      title: 'Ask',
      contexts: ['selection']
  });
  chrome.contextMenus.create({
      id: 'summarize',
      parentId: 'aiExtension',
      title: 'Summarize',
      contexts: ['selection']
  });
  chrome.contextMenus.create({
      id: 'explain',
      parentId: 'aiExtension',
      title: 'Explain',
      contexts: ['selection']
  });
});

chrome.contextMenus.onClicked.addListener((info, tab) => {
  const selectedText = info.selectionText;
  chrome.storage.local.set({ selectedText }, () => {
      chrome.windows.create({
          url: chrome.runtime.getURL('index.html'),
          type: 'popup',
          width: 350,
          height: 450
      });
  });
});

function fetchWithAuth(url, options, sendResponse) {
  chrome.storage.local.get('token', (result) => {
      if (result.token) {
          options.headers = options.headers || {};
          options.headers['Authorization'] = 'Bearer ' + result.token;
      }

      fetch(url, options)
      .then(response => response.json())
      .then(data => sendResponse(data)
    
    )
      .catch(error => sendResponse({ error: 'Failed to perform the request.' }));
  });
}

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if (message.type === 'PUSH_CONTENT') {
      fetchWithAuth('http://localhost:3000/push', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({ pageContent: message.pageContent, url: message.url })
      }, sendResponse);
      return true;
  }
  if (message.type === 'SUMMARIZE_CONTENT') {
      fetchWithAuth('http://localhost:3000/summarize', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({ pageContent: message.pageContent, url: message.url })
      }, sendResponse);
      return true;
  }
  if (message.type === 'EXPLAIN_CONTENT') {
      fetchWithAuth('http://localhost:3000/explain', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({ userInput: message.userInput, pageContent: message.pageContent })
      }, sendResponse);
      return true;
  }
  if (message.type === 'ASK_QUESTION') {
    fetchWithAuth('http://localhost:3000/ask', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ userInput: message.userInput, url: message.url})
    }, sendResponse);
    return true;
}

  if (message.type === 'signup') {
      fetch('http://localhost:3000/signup', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({ email: message.email, password: message.password })
      })
      .then(response => response.json())
      .then(data => sendResponse(data))
      .catch(error => sendResponse({ error: 'Signup failed.' }));
      return true;
  }
  if (message.type === 'signinButton') {
      fetch('http://localhost:3000/signin', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({ email: message.email, password: message.password })
      })
      .then(response => response.json())
      .then(data => {
          if (data.token) {
              chrome.storage.local.set({ token: data.token }, () => {
                  sendResponse(data);
              //    console.log(data)
              });
          } else {
              sendResponse({ error: 'Login failed.' });
          }
      })
      .catch(error => sendResponse({ error: 'Login failed.' }));
      return true;
  }
});

  
  
  
  
  
  

