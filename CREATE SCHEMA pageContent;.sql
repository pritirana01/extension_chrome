CREATE SCHEMA pageContent;

CREATE TABLE pageContent.pageContentTable(
    id SERIAL PRIMARY KEY,
    content TEXT;
);