const { createClient } = require('@supabase/supabase-js');
const supabaseUrl = 'https://tvlccjaippvrqayompmg.supabase.co';
const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InR2bGNjamFpcHB2cnFheW9tcG1nIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTgxODQ4MDcsImV4cCI6MjAzMzc2MDgwN30.m0Ogv55ryqj1e5uBKtLa73LJQdS9qpTMnYwnYqKyYeQ';
const supabase = createClient(supabaseUrl, supabaseKey); 

async function authMiddleware(req, res, next) {
  const token = req.headers['authorization']?.split(' ')[1];

  if (!token) {
    return res.status(401).json({ error: 'Authorization token required' });
  }

  try {
    const { data, error } = await supabase.auth.getUser(token);

    if (error || !data) {
      return res.status(401).json({ error: 'Invalid token' });
    }
    // const authid=data.user.id
    // console.log(authid)
    req.user = data.user;
    next();
  } catch (error) {
    res.status(500).json({ error: 'Failed to authenticate token' });
  }
}

module.exports = authMiddleware;
