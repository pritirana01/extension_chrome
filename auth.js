

document.addEventListener('DOMContentLoaded', () => {
  const authContainer = document.getElementById('authContainer');
  const mainContainer = document.getElementById('mainContainer');
  const signupContainer = document.getElementById('signupContainer');
  const signinContainer = document.getElementById('signinContainer');

  const showLoginLink = document.getElementById('showlogin');
  const showSignupLink = document.getElementById('showsignup');

  // Event listeners for switching between signup and signin forms
  showLoginLink.addEventListener('click', (event) => {
    event.preventDefault();
    signupContainer.style.display = 'none';
    signinContainer.style.display = 'block';
  });

  showSignupLink.addEventListener('click', (event) => {
    event.preventDefault();
    signinContainer.style.display = 'none';
    signupContainer.style.display = 'block';
  });

  // Event listener for signup button
 const testBUtton= document.getElementById('signupButton')
 console.log(testBUtton)
 testBUtton.addEventListener('click', () => {
    const email = document.getElementById('signupEmail').value;
    const password = document.getElementById('signupPassword').value;

    chrome.runtime.sendMessage({ type: 'signupButton', email, password }, response => {
      if (response.error) {
        alert('Something went wrong');
      } else {
        alert(response.message);
      }
    });
  });

  // Event listener for signin button
  document.getElementById('signinButton').addEventListener('click', () => {
    const email = document.getElementById('signinEmail').value;
    const password = document.getElementById('signinPassword').value; 

    chrome.runtime.sendMessage({ type: 'signinButton', email, password }, response => {
      if (response.error) {
        alert('Login failed. Please check your credentials.');
      } else {
        authContainer.style.display = 'none';
        mainContainer.style.display = 'block';
        //mainContainer.style.height='200px';
     //   document.body.style.height='200px';
      }
    });
  });

  // Event listener for logout button
  document.getElementById('logoutButton').addEventListener('click', () => {
    chrome.storage.local.remove('authToken', () => {
      authContainer.style.display = 'block';
      mainContainer.style.display = 'none';
      signupContainer.style.display = 'block';
      signinContainer.style.display = 'none';
    });
  });
  
  });
  