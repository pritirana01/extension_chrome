
    async function generateEmbeddings(chunk) {
        const fetch = (await import('node-fetch')).default;
        const promises = chunk.map(chunk =>
            fetch('https://api.openai.com/v1/embeddings', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${OPENAI_API_KEY}`
                },
                body: JSON.stringify({
                    input: chunk,
                    model: 'text-embedding-ada-002'
                })
            }).then(response => response.json())
              .then(data => data.data[0].embedding)
        );
        const embeddings = await Promise.all(promises);
        return embeddings;
    }

module.exports = { generateEmbeddings };