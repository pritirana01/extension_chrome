
document.addEventListener('DOMContentLoaded', () => {
  chrome.storage.local.get('selectedText',(result)=> {
   // console.log("testing------->",chrome.storage.local)
     if (result.selectedText) {
      document.getElementById('pageContent').value = result.selectedText;
      chrome.storage.local.remove('selectedText');
    } else {
      chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
        const url = tabs[0].url;
        chrome.scripting.executeScript(
          {
            target: { tabId: tabs[0].id },
            func: () => document.body.innerText,
          },
          (results) => {
            if (results && results[0]) {
              const pageContent = results[0].result.trim();
              if (pageContent && url) {
                chrome.runtime.sendMessage({ type: 'PUSH_CONTENT', pageContent, url }, (response) => {
                  document.getElementById('response').innerText = response.response || response.error;
                  document.getElementById('pageContent').value = pageContent;
                });
              } else {
                console.error('URL or page content is missing.');
              }
            }
          }
        );
      });
    }
  });
});
const getPushBUtton=document.getElementById('pushContent')
addEventListener('click', () => {
  const pageContent = document.getElementById('pageContent').value;
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    const url = tabs[0].url;
    if (pageContent && url) {
      chrome.runtime.sendMessage({ type: 'PUSH_CONTENT', pageContent, url }, response => {
        document.getElementById('response').innerText = response.response || response.error;
      });
    } else {
      console.error('URL or page content is missing.');
    }
  });
});

document.getElementById('summarizeContent').addEventListener('click', () => {
  const pageContent = document.getElementById('pageContent').value;
  chrome.runtime.sendMessage({ type: 'SUMMARIZE_CONTENT', pageContent }, response => {
    document.getElementById('response').innerText = response.response || response.error;
  });
});

document.getElementById('explainContent').addEventListener('click', () => {
  const pageContent = document.getElementById('pageContent').value;
  const userInput = prompt('Enter the text you need explained:');
  if (userInput) {
    chrome.runtime.sendMessage({ type: 'EXPLAIN_CONTENT', userInput, pageContent }, response => {
      document.getElementById('response').innerText = response.response || response.error;
    });
  } else {
    console.error('User input is missing.');
  }
});

document.getElementById('askQuestion').addEventListener('click', () => {
  const userInput =document.createElement('input')
  //const userInput = prompt('Enter your question:');
  if (userInput) {
    chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
      const url = tabs[0].url;
      if (url) {
        chrome.runtime.sendMessage({ type: 'ASK_QUESTION', userInput, url }, response => {
          if (response.error) {
            document.getElementById('response').innerText = response.error;
          } else {
            document.getElementById('response').innerText = response.response || 'No valid response received.';
          }
        });
      } else {
        console.error('URL is missing.');
      }
    });
  } else {
    console.error('User input is missing.');
  }
});

  // Listen for messages from the background script
  /*chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.action === 'popup') {
        // Check if the iframe already exists
        if (!document.getElementById('aiPopupIframe')) {
            // Create an iframe for the popup
            const iframe = document.createElement('iframe');
            iframe.id = 'aiPopupIframe';
            iframe.src = chrome.runtime.getURL('index.html');
            iframe.style.position = 'fixed';
            iframe.style.top = '10%';
            iframe.style.left = '50%';
            iframe.style.transform = 'translateX(-50%)';
            iframe.style.width = '350px';
            iframe.style.height = '450px';
            iframe.style.zIndex = '9999';
            iframe.style.border = '1px solid #ccc';
            iframe.style.backgroundColor = 'white';
  
            // Append the iframe to the body
            document.body.appendChild(iframe);
  
            // When the iframe loads, send a message to load the text
            iframe.onload = function () {
                iframe.contentWindow.postMessage({ action: 'loadText' }, '*');
            };
        }
    }
  });
  
  window.addEventListener('message', (event) => {
    if (event.data.action === 'loadText') {
        chrome.storage.local.get(['selectedText', 'action']).then((result) => {
            const selectedText = result.selectedText;
            if (selectedText) {
                document.getElementById('pageContent').value = selectedText;
            }
        });
    }
  });
  
  document.addEventListener('DOMContentLoaded', () => {
    // Handle button clicks
    document.getElementById('summarizeContent').addEventListener('click', () => {
        const text = document.getElementById('pageContent').value;
        sendTextToServer('summarize', text);
    });
  
    document.getElementById('explainContent').addEventListener('click', () => {
        const text = document.getElementById('pageContent').value;
        sendTextToServer('explain', text);
    });
  
    document.getElementById('askQuestion').addEventListener('click', () => {
        const text = document.getElementById('pageContent').value;
        sendTextToServer('ask', text);
    });
  });*/
  
  /*function sendTextToServer(action, text) {
    if (!text) {
        alert("No content provided to process.");
        return;
    }
  
    let url;
    let requestBody;
    switch (action) {
        case 'explain':
            url = 'http://localhost:3000/explain';
            requestBody = { userInput: text, pageContent: text };
            break;
        case 'summarize':
            url = 'http://localhost:3000/summarize';
            requestBody = { pageContent: text };
            break;
        case 'ask':
            url = 'http://localhost:3000/ask';
            requestBody = { userInput: text, url: text };
            break;
        default:
            return;
    }
  
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(requestBody)
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(responseData => {
        document.getElementById('response').innerText = responseData.response;
    })
    .catch(error => {
        console.error('Error sending text to server:', error);
    });
  }*/
  
  
  
  
  
  
  
  
  