// const express = require('express');
// const { Pool } = require('pg');
// const cors = require('cors');
// const { createClient } = require('@supabase/supabase-js');
// const bcrypt = require('bcrypt');

// const app = express();
// const PORT = 3000;
// const OPENAI_API_KEY = 'sk-proj-wCGfYiF2lYWuDD7VMGj0T3BlbkFJbLacmIODHiMh89u9dltE';

// const supabaseUrl = 'https://tvlccjaippvrqayompmg.supabase.co';
// const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InR2bGNjamFpcHB2cnFheW9tcG1nIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTgxODQ4MDcsImV4cCI6MjAzMzc2MDgwN30.m0Ogv55ryqj1e5uBKtLa73LJQdS9qpTMnYwnYqKyYeQ';
// const supabase = createClient(supabaseUrl, supabaseKey);

// const pool = new Pool({
//   user: 'userdb',
//   host: 'localhost',
//   database: 'dbname',
//   password: '123qwer',
//   port: 5432,
// });

// app.use(cors());
// app.use(express.json());
// app.use(express.static('index.html'))



function cosineSimilarity(vecA, vecB) {
  let dotProduct = 0;
  let normA = 0;
  let normB = 0;

  for (let i = 0; i < vecA.length; i++) {
    dotProduct += vecA[i] * vecB[i];
    normA += vecA[i] * vecA[i];
    normB += vecB[i] * vecB[i];
  }

  return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
}

async function getEmbedding(text) {
  try {
    const response = await fetch('https://api.openai.com/v1/embeddings', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`,
      },
      body: JSON.stringify({
        model: 'text-embedding-ada-002',
        input: text,
      }),
    });
    const data = await response.json();
    console.log('Embedding data:', data);
    if (data.data && data.data.length > 0 && data.data[0].embedding) {
      return data.data[0].embedding;
    } else {
      throw new Error('Invalid response from OpenAI API');
    }
  } catch (error) {
    console.error('Error fetching embedding data:', error);
    throw new Error('Failed to fetch embedding data');
  }
}

function funchunk(text, size = 100) {
  const sentences = text.match(/[^.!?]+[.!?]+[\])'"`’”]*/g) || [];
  let chunks = [];
  let chunk = '';

  for (let sentence of sentences) {
    if ((chunk + sentence).length > size) {
      chunks.push(chunk.trim());
      chunk = '';
    }
    chunk += sentence + ' ';
  }

  if (chunk) {
    chunks.push(chunk.trim());
  }
  console.log('Chunks:', chunks);
  return chunks;
}

async function generateEmbeddings(chunks) {
  const promises = chunks.map(chunk =>
    fetch('https://api.openai.com/v1/embeddings', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`,
      },
      body: JSON.stringify({
        input: chunk,
        model: 'text-embedding-ada-002',
      }),
    })
      .then(response => response.json())
      .then(data => {
        console.log('Chunk embedding:', data);
        return data.data[0].embedding;
      })
  );
  const embeddings = await Promise.all(promises);
  return embeddings;
}

// app.post('/push', async (req, res) => {
//   const { pageContent, url } = req.body;
//   console.log('Received /push request:', req.body);

//   if (!pageContent || !url) {
//     return res.status(400).json({ error: 'Content and URL are required.' });
//   }

//   try {
//     const result = await pool.query('SELECT 1 FROM dataContent.dataTable WHERE url = $1', [url]);
//     if (result.rows.length > 0) {
//       return res.status(400).json({ error: 'Content for this URL already exists in the database.' });
//     }

//     const callChunk = funchunk(pageContent);
//     const genEmbedding = await generateEmbeddings(callChunk);

//     await pool.query('BEGIN');
//     for (let i = 0; i < callChunk.length; i++) {
//       await pool.query('INSERT INTO dataContent.dataTable (content, embedded, url) VALUES ($1, $2, $3)', [
//         callChunk[i],
//         JSON.stringify(genEmbedding[i]),
//         url,
//       ]);
//     }
//     await pool.query('COMMIT');

//     res.json({ response: 'Content pushed successfully.' });
//   } catch (error) {
//     console.error('Error inserting content:', error);
//     await pool.query('ROLLBACK');
//     res.status(500).json({ error: 'Failed to push content.' });
//   }
// });

// app.post('/summarize', async (req, res) => {
//   const { pageContent } = req.body;
//   console.log('Received /summarize request:', req.body);
//   const prompt = `Summarize the following content: ${pageContent}`;

//   try {
//     const response = await fetch('https://api.openai.com/v1/chat/completions', {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//         'Authorization': `Bearer ${OPENAI_API_KEY}`,
//       },
//       body: JSON.stringify({
//         model: 'gpt-4',
//         messages: [
//           { role: 'system', content: 'You are a helpful assistant.' },
//           { role: 'user', content: prompt },
//         ],
//       }),
//     });
//     const data = await response.json();
//     console.log('Summary data:', data.choices[0].message);
//     res.json({ response: data.choices[0].message.content });
//   } catch (error) {
//     res.status(500).json({ error: 'Failed to fetch AI response.', error });
//   }
// });

// app.post('/explain', async (req, res) => {
//   const { userInput, pageContent } = req.body;
//   console.log('Received /explain request:', req.body);
//   const prompt = `Explain this in simple terms: "${userInput}" from the context of: ${pageContent}`;

//   try {
//     const response = await fetch('https://api.openai.com/v1/chat/completions', {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//         'Authorization': `Bearer ${OPENAI_API_KEY}`,
//       },
//       body: JSON.stringify({
//         model: 'gpt-4',
//         messages: [
//           { role: 'system', content: 'You are a helpful assistant.' },
//           { role: 'user', content: prompt },
//         ],
//       }),
//     });
//     const data = await response.json();
//     console.log('Explain data:', data);
//     res.json({ response: data.choices[0].message.content });
//   } catch (error) {
//     res.status(500).json({ error: 'Failed to fetch AI response.' });
//   }
// });

// app.post('/ask', async (req, res) => {
//   const { userInput, url } = req.body;
//   console.log('Received /ask request:', req.body);

//   try {
//     const userInputEmbedding = await getEmbedding(userInput);
//     const result = await pool.query('SELECT content, embedded FROM dataContent.dataTable WHERE url = $1', [url]);

//     if (result.rows.length === 0) {
//       return res.status(404).json({ error: 'Content not found for the provided URL.' });
//     }

//     const contents = result.rows.map(row => {
//       console.log('Row:', row);
//       return {
//         content: row.content,
//         embedding: typeof row.embedded === 'string' ? JSON.parse(row.embedded.trim()) : row.embedded, 
//       };
//     });

//     const rankedResults = contents.map(({ content, embedding }) => ({
//       content,
//       score: cosineSimilarity(userInputEmbedding, embedding),
//     })).sort((a, b) => b.score - a.score);

//     const mostRelevantContent = rankedResults[0]?.content;

//     const prompt = `Based on the following context, answer this question: "${userInput}"\n\nContext: ${mostRelevantContent}`;
//     const response = await fetch('https://api.openai.com/v1/chat/completions', {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//         'Authorization': `Bearer ${OPENAI_API_KEY}`,
//       },
//       body: JSON.stringify({
//         model: 'gpt-4',
//         messages: [
//           { role: 'system', content: 'You are a helpful assistant.' },
//           { role: 'user', content: prompt },
//         ],
//       }),
//     });

//     const data = await response.json();
//     const aiResponse = data.choices[0].message.content;
//     console.log('Ask data:', data);
//     res.json({ response: aiResponse });
//   } catch (error) {
//     console.error('Error processing request:', error);
//     res.status(500).json({ error: 'Failed to process request.' });
//   }
// });

// app.post('/signup', async (req, res) => {
//   try {
//     const { email, password } = req.body;

//     if (!email || !password) {
//       return res.status(400).json({ error: 'Email and password are required' });
//     }

//     console.log('Incoming request:', { email, password });

//     const { data, error } = await supabase.auth.signUp({
//       email: email,
//       password: password,
//     });

//     if (error) {
//       throw new Error(error.message);
//     }

//     await supabase.from('auth.users').update({ email_confirmed_at: new Date() }).eq('email', email);

//     res.status(201).json({ message: 'User registered successfully and email confirmed', user: data.user });
//   } catch (error) {
//     console.error('Error during signup:', error.message);
//     res.status(500).json({ error: error.message });
//   }
// });

// app.post('/signin', async (req, res) => {
//   try {
//     const { email, password } = req.body;
//     console.log(req.body);

//     if (!email || !password) {
//       return res.status(400).json({ error: 'Email and password are required' });
//     }

//     console.log('Incoming request:', { email, password });

//     const { data, error } = await supabase.auth.signInWithPassword({
//       email: email,
//       password: password,
//     });

//     if (error) {
//       throw new Error(error.message);
//     }

//     const { user, session } = data;

//     if (!session) {
//       return res.status(400).json({ error: 'Authentication failed, no session returned' });
//     }

//     const token = session.access_token;

//     res.status(200).json({ message: 'User signed in successfully', user, token });
//   } catch (error) {
//     console.error('Error during signin:', error.message);
//     res.status(500).json({ error: error.message });
//   }
// });


// app.listen(PORT, () => {
//   console.log(`Server is running on port ${PORT}`);
// });

// server.js
const express = require('express');
const { Pool } = require('pg');
const cors = require('cors');
//const { createClient } = require('@supabase/supabase-js');
const bcrypt = require('bcrypt');
const authMiddleware = require('../authToken');
const rateLimitFun=require('./rateMiddleware')
const checkRateLimit=require('./checkLimit')
const supabase=require('./supaBaseClient')
const dotenv = require('dotenv');

dotenv.config();

const app = express();
const PORT = process.env.PORT || 3000;
const OPENAI_API_KEY = process.env.OPENAI_API_KEY;

/*const supabaseUrl = process.env.SUPABASE_URL;
const supabaseKey = process.env.SUPABASE_KEY;
const supabase = createClient(supabaseUrl, supabaseKey);*/

/*const pool = new Pool({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
});*/
console.log(supabase)
app.use(cors());
app.use(express.json());

// Add the auth middleware to routes that require authentication
app.post('/push', authMiddleware, async (req, res) => {
  const { pageContent, url } = req.body;
  console.log('Received /push request:', req.body);

  if (!pageContent || !url) {
    return res.status(400).json({ error: 'Content and URL are required.' });
  }
  try {
    const { data, error } = await supabase
      .from('contents')
      .select('*')
      .eq('url', url);

    if (error) {
      throw error;
    }

    if (data.length > 0) {
      return res.status(400).json({ error: 'Content for this URL already exists in the database.' });
    }

    const callChunk = funchunk(pageContent);
    const genEmbedding = await generateEmbeddings(callChunk);

    const insertData = callChunk.map((chunk, index) => ({
      content: chunk,
      embedded: genEmbedding[index],
      url: url,
    }));

    const { insertError } = await supabase
      .from('contents')
      .insert(insertData);

    if (insertError) {
      throw insertError;
    }

    res.json({ response: 'Content pushed successfully.' });
  } catch (error) {
    console.error('Error inserting content:', error);
    res.status(500).json({ error: 'Failed to push content.' });
  }
});

// Use the auth middleware for other routes as needed
app.post('/summarize', authMiddleware, rateLimitFun, checkRateLimit,async (req, res) => {
  const { pageContent } = req.body;
  console.log('summarize request:', req.body);
  const prompt = `Summarize the following content: ${pageContent}`;

  try {
    const response = await fetch('https://api.openai.com/v1/chat/completions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`,
      },
      body: JSON.stringify({
        model: 'gpt-4',
        messages: [
          { role: 'system', content: 'You are a helpful assistant.' },
          { role: 'user', content: prompt },
        ],
      }),
    });
    const data = await response.json();
    console.log('Summary data:', data.choices[0].message);
    res.json({ response: data.choices[0].message.content });
  } catch (error) {
    res.status(500).json({ error: 'Failed to fetch AI response.', error });
  }
});

app.post('/explain', authMiddleware, async (req, res) => {
  const { userInput, pageContent } = req.body;
  console.log('Received /explain request:', req.body);
  const prompt = `Explain this in simple terms: "${userInput}" from the context of: ${pageContent}`;

  try {
    const response = await fetch('https://api.openai.com/v1/chat/completions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`,
      },
      body: JSON.stringify({
        model: 'gpt-4',
        messages: [
          { role: 'system', content: 'You are a helpful assistant.' },
          { role: 'user', content: prompt },
        ],
      }),
    });
    const data = await response.json();
    console.log('Explain data:', data);
    res.json({ response: data.choices[0].message.content });
  } catch (error) {
    res.status(500).json({ error: 'Failed to fetch AI response.' });
  }
});

app.post('/ask', authMiddleware,rateLimitFun, async (req, res) => {
  const { userInput, url } = req.body;
  console.log('Received /ask request:', req.body);

  try {
    const userInputEmbedding = await getEmbedding(userInput);
    const { data: contents, error } = await supabase
      .from('contents')
      .select('content, embedded')
      .eq('url', url);

    if (error) {
      console.error('Error fetching data from Supabase:', error);
      return res.status(500).json({ error: 'Failed to fetch data from Supabase.' });
    }

    if (!contents || contents.length === 0) {
      return res.status(404).json({ error: 'Content not found for the provided URL.' });
    }

    const contentEmbeddings = contents.map(row => ({
      content: row.content,
      embedding: typeof row.embedded === 'string' ? JSON.parse(row.embedded.trim()):row.embedded,
    }));

    const rankedResults = contentEmbeddings.map(({ content, embedding }) => ({
      content,
      score: cosineSimilarity(userInputEmbedding, embedding),
    })).sort((a, b) => b.score - a.score);

    const mostRelevantContent = rankedResults[0]?.content;

    const prompt = `Based on the following context, answer this question: "${userInput}"\n\nContext: ${mostRelevantContent}`;
    const response = await fetch('https://api.openai.com/v1/chat/completions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`,
      },
      body: JSON.stringify({
        model: 'gpt-4',
        messages: [
          { role: 'system', content: 'You are a helpful assistant.' },
          { role: 'user', content: prompt },
        ],
      }),
    });

    const responseData = await response.json();
    const aiResponse = responseData.choices[0].message.content;
    console.log('Ask data:', responseData);
    res.json({ response: aiResponse });
  } catch (error) {
    console.error('Error processing request:', error);
    res.status(500).json({ error: 'Failed to process request.' });
  }
});


/*app.post('/ask', authMiddleware, async (req, res) => {
  const { userInput, url } = req.body;
  console.log('Received /ask request:', req.body);

  try {
    const userInputEmbedding = await getEmbedding(userInput);
    const result = await pool.query('SELECT content, embedded FROM dataContent.dataTable WHERE url = $1', [url]);

    if (result.rows.length === 0) {
      return res.status(404).json({ error: 'Content not found for the provided URL.' });
    }

    const contents = result.rows.map(row => {
      console.log('Row:', row);
      return {
        content: row.content,
        embedding: typeof row.embedded === 'string' ? JSON.parse(row.embedded.trim()) : row.embedded, 
      };
    });

    const rankedResults = contents.map(({ content, embedding }) => ({
      content,
      score: cosineSimilarity(userInputEmbedding, embedding),
    })).sort((a, b) => b.score - a.score);

    const mostRelevantContent = rankedResults[0]?.content;

    const prompt = `Based on the following context, answer this question: "${userInput}"\n\nContext: ${mostRelevantContent}`;
    const response = await fetch('https://api.openai.com/v1/chat/completions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${OPENAI_API_KEY}`,
      },
      body: JSON.stringify({
        model: 'gpt-4',
        messages: [
          { role: 'system', content: 'You are a helpful assistant.' },
          { role: 'user', content: prompt },
        ],
      }),
    });

    const data = await response.json();
    const aiResponse = data.choices[0].message.content;
    console.log('Ask data:', data);
    res.json({ response: aiResponse });
  } catch (error) {
    console.error('Error processing request:', error);
    res.status(500).json({ error: 'Failed to process request.' });
  }
});*/

app.post('/signup', async (req, res) => {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      return res.status(400).json({ error: 'Email and password are required' });
    }

    console.log('Incoming request:', { email, password });

    const { data, error } = await supabase.auth.signUp({
      email: email,
      password: password,
    });

    if (error) {
      throw new Error(error.message);
    }

    await supabase.from('auth.users').update({ email_confirmed_at: new Date() }).eq('email', email);

    res.status(201).json({ message: 'User registered successfully and email confirmed', user: data.user });
  } catch (error) {
    console.error('Error during signup:', error.message);
    res.status(500).json({ error: error.message });
  }
});

app.post('/signin', async (req, res) => {
  try {
    const { email, password } = req.body;
    console.log(req.body);

    if (!email || !password) {
      return res.status(400).json({ error: 'Email and password are required' });
    }

    console.log('Incoming request:', { email, password });

    const { data, error } = await supabase.auth.signInWithPassword({
      email: email,
      password: password,
    });

    if (error) {
      throw new Error(error.message);
    }

    const { user, session } = data;

    if (!session) {
      return res.status(400).json({ error: 'Authentication failed, no session returned' });
    }

    const token = session.access_token;

    res.status(200).json({ message: 'User signed in successfully', user, token });
  } catch (error) {
    console.error('Error during signin:', error.message);
    res.status(500).json({ error: error.message });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
module.exports=app
