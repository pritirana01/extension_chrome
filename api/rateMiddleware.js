
/*const supabase=require('./supaBaseClient')
async function rateLimitFun(req,res,next){
    try{
      const userId=req.user.id //extracting user id
    //  console.log(user)
      const endpoint=req.url //geting endpoint
      const {error}=await supabase.from('userlimit').insert([{userid:userId,endpoint}])
      if(error) throw error 
      next()
     
    }
    catch(err){
     console.log(err)
     res.status(501).json({message:"some error occured"})
    }   


}

module.exports = rateLimitFun;*/
const supabase = require('./supaBaseClient');

const RATE_LIMIT = 100; // Define your rate limit here

// Middleware to track visit counts for individual endpoints
async function rateLimitFun(req, res, next) {
    try {
        const userId = req.user.id; // Extracting user id
        console.log("User ID:", userId);
        const endpoint = req.url; // Getting endpoint
        console.log("Endpoint:", endpoint);

        // Check if the user and endpoint record exists
        const { data, error: selectError, status } = await supabase
            .from('userlimit')
            .select('visitcount')
            .eq('userid', userId)
            .eq('endpoint', endpoint)
            .single();

        if (selectError && status !== 406) { // 406 means no record found
            throw selectError;
        }

        console.log("Data from select query:", data);

        if (data) {
            // If record exists, increment the visit count
            const { error: updateError } = await supabase
                .from('userlimit')
                .update({ visitcount: data.visitcount + 1 })
                .eq('userid', userId)
                .eq('endpoint', endpoint);

            if (updateError) {
                throw updateError;
            }

            console.log(`Updated visit count to ${data.visitcount + 1} for user ${userId} on endpoint ${endpoint}`);
        } else {
            // If record does not exist, insert a new record with visitcount = 1
            const { error: insertError } = await supabase
                .from('userlimit')
                .insert([{ userid: userId, endpoint, visitcount: 1 }]);

            if (insertError) {
                throw insertError;
            }

            console.log(`Inserted new record for user ${userId} on endpoint ${endpoint} with visit count 1`);
        }

        next();
    } catch (err) {
        console.log("Error:", err);
        res.status(501).json({ message: "Some error occurred", err });
    }
}



module.exports = rateLimitFun;




