const supabase = require('./supaBaseClient');

const RATE_LIMIT =100;
async function checkRateLimit(req, res, next) {
    try {
        const userId = req.user.id; // Extracting user id
        console.log("User ID for rate limit check:", userId);

        // Get total visit count for the user  all endpoints
        const { data, error: selectError } = await supabase
            .from('userlimit')
            .select('visitcount')
            .eq('userid', userId);

        if (selectError) {
            throw selectError;
        }

        const totalVisits = data.reduce((sum, record) => sum + record.visitcount, 0);

        console.log(`Total visit count for user ${userId}: ${totalVisits}`);

        if (totalVisits >= RATE_LIMIT) {
            res.status(429).json({ message: "Your limit has been reached." });
        } else {
            next();
        }
    } catch (err) {
        console.log("Error:", err);
        res.status(501).json({ message: "Some error occurred", err });
    }
}
module.exports=checkRateLimit